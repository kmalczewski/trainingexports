<?php

namespace ExportsBundle\Services;

/**
 * Description of Cars
 *
 * @author kaziu
 */
class Cars
{
    /**
     *
     * @var type array
     */
    private $cars;

    public function __construct()
    {
        $this->cars = array(
            array(
                "make" => 'fake_make_2',
                "model" => 'fake_model_3',
                "body_type" => 'fake_body_model_4',
                "price" => 50,
                "color" => 'blue',
            ),
            array(
                "make" => 'fake_make_1',
                "model" => 'fake_model_1',
                "body_type" => 'fake_body_model_1',
                "price" => 200,
                "color" => 'red',
            ),
            array(
                "make" => 'fake_make',
                "model" => 'fake_model',
                "body_type" => 'fake_body_model',
                "price" => 100,
                "color" => 'green',
            ),
        );
    }

    public function setCars($arrayCars = array())
    {
        $this->cars = $arrayCars;

        return $this;
    }

    public function getCars()
    {
        usort($this->cars,
            function($firstElement, $secondElement) {
            if ($firstElement['price'] == $secondElement['price']) {
                return 0;
            }
            return ($firstElement['price'] > $secondElement['price']) ? -1 : 1;
        });

        return $this->cars;
    }
}